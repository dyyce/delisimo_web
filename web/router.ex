defmodule Delisimo.Router do
  use Delisimo.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Delisimo do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/results", PageController, :results
  end

  # Other scopes may use custom stacks.
  # scope "/api", Delisimo do
  #   pipe_through :api
  # end
end
