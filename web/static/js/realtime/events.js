import socket from '../socket'

// init data comes from socket.js
export const setupRealtime = (store, actions) => {
  let state = store.getState()

  store.dispatch(actions.sortByRating({}))

  let channel = socket.channel("page:results", {})

  channel.on("res_results", payload => {
    store.dispatch(actions.getResultsSuccess(payload))
  })
  channel.on("filter_changed", payload => {
    store.dispatch(actions.distanceFilterChangedSuccess(payload.results, payload.user_location))
  })
}
