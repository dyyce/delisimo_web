import ResultsBar from './ResultsBar'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../../actions/results_actions'

export default connect(
  state => {
    return ({
      results: state.resultsReducer.results,
      user_location: state.resultsReducer.user_location
    })
  },
  dispatch => bindActionCreators(actions, dispatch)
)(ResultsBar)
