import React, { Component, PropTypes } from 'react'
import GoogleMap from 'google-map-react';
import _ from 'lodash'
import ReactToolTip from 'react-tooltip'
import GeosuggestInput from '../GeosuggestInput'

import Markers from '../Markers'

class ResultsBar extends Component {
  componentWillMount() {
    this.state = {
      showMap: false,
      showSearch: false,
      showSort: false,

      results: this.props.results,
      user_location: this.props.user_location,
      mapCenter: {
        lat: parseFloat(this.props.user_location.lat),
        lng: parseFloat(this.props.user_location.lng)
      }
    }
  }

  componentDidMount() {
  }

  _onChildMouseEnter(key, childProps) {

  }

  _onChildMouseLeave(key, childProps) {

  }

  _onBoundsChanged(center, zoom, bounds, marginBounds) {

  }

  _onChildClick(key, childProps) {
    this.setState({
      mapCenter: childProps
    })
  }

  _handleOnSortRating() {
    this.props.sortByRating()
  }

  renderMap() {

    return <div style={{ height: 430, width: '100%' }}>
      <GoogleMap style={{height: 350, width: '100%', top: 51, position: 'relative'}}
        defaultCenter={{
          lat: parseFloat(this.props.user_location.lat),
          lng: parseFloat(this.props.user_location.lng)
        }}
        defaultZoom={16}
        bootstrapURLKeys={{
          key: 'AIzaSyBXTR2titi7TnK231AI2pEs1rPyLEbTzgg',
          language: 'de',
        }}
        center={{
          lat: parseFloat(this.props.user_location.lat),
          lng: parseFloat(this.props.user_location.lng)
        }}
        onChildMouseEnter={this._onChildMouseEnter.bind(this)}
        onChildMouseLeave={this._onChildMouseLeave.bind(this)}
        onBoundsChanged={this._onBoundsChanged.bind(this)}
        onChildClick={this._onChildClick.bind(this)}
        >

        <Markers.MainMarker
          lat={parseFloat(this.props.user_location.lat)}
          lng={parseFloat(this.props.user_location.lng)}
          data-tip="Me tooltip"
          >
          </Markers.MainMarker>

        {this.props.results.map((item, key) => {
          return <Markers.ResultMarker
            data-tip={item.name}
            key={key}
            lat={parseFloat(item.lat)}
            lng={parseFloat(item.lng)}
            item={item}
            >
            </Markers.ResultMarker>
        })}
      </GoogleMap>

      <ReactToolTip effect="solid"></ReactToolTip>
    </div>
  }

  renderSearch() {
    return <div className="col-sm-4 col-md-6 col-lg-6 col-sm-offset-2 col-lg-offset-3">
      <GeosuggestInput style={{zIndex: 9999}} />
    </div>
  }

  renderSort() {
    return <div>
      <button className="btn btn-default" onClick={this._handleOnSortRating.bind(this)}>Nach Bewertung</button>
    </div>
  }

  render() {
    return <div className="col-sm-10 col-sm-offset-1">
      <div>
        <div className="col-sm-3 results-bar-item back-item" style={{
          margin: 'auto 0',
          textAlign: 'center'
        }}
        onClick={() => window.location.href = '/'}
        >
          {/* <a href="/" className="results-bar-item back-item"
            > */}
            <i style={{
              fontSize: '1.3rem'
            }} className="fa fa-chevron-left" aria-hidden="true"></i> Zurück
          {/* </a> */}
        </div>
        <div className="col-sm-3 sort-item results-bar-item"
          onClick={() => this.setState({showSort: !this.state.showSort, showSearch: false, showMap: false})}>
          <i style={{
            fontSize: '1.3rem'
          }} className="fa fa-sort" aria-hidden="true"></i> Sortieren
        </div>
        <div className="col-sm-3 search-item results-bar-item"
          onClick={() => this.setState({showSearch: !this.state.showSearch, showSort: false, showMap: false})}>
          <i style={{
            fontSize: '1.3rem'
          }} className="fa fa-search" aria-hidden="true"></i> Suche
        </div>
        <div className="col-sm-3 map-item results-bar-item"
          onClick={() => this.setState({ showMap: !this.state.showMap, showSearch: false, showSort: false })}
          >
          <i style={{
            fontSize: '1.3rem'
          }} className="fa fa-map" aria-hidden="true"></i> Karte
        </div>
      </div>
      {this.state.showMap ? this.renderMap() : ''}
      {this.state.showSearch ? this.renderSearch() : ''}
      {this.state.showSort ? this.renderSort() : ''}
    </div>
  }
}

export default ResultsBar
