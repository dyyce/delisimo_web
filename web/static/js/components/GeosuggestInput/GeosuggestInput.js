import React, { Component } from 'react'
import Geosuggest from 'react-geosuggest'

let fixtures = [
  {label: 'Berlin', location: {lat: 52.5200, lng: 13.4050}},
  {label: 'Hamburg', location: {lat: 53.5511, lng: 9.9937}},
  {label: 'München', location: {lat: 48.1351, lng: 11.5820}},
  {label: 'Würzburg', location: {lat: 49.7913, lng: 9.9534}},
  {label: 'Aschaffenburg', location: {lat: 49.9807, lng: 9.1356}}
];

const default_radius = 400

class GeosuggestInput extends Component {
  componentWillMount() {
    this.state = {
      showSpinner: false
    }
  }

  forwardToResults(lat, lng) {
    window.location.replace(`/results?lat=${lat}&lng=${lng}&radius=600`)
  }

  handleOnClick(e) {
    this.setState({
      showSpinner: true
    })

    var options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumWait: 10000,     // max wait time for desired accuracy
      maximumAge: 0,          // disable cache
      desiredAccuracy: 30,    // meters
      fallbackToIP: true,     // fallback to IP if Geolocation fails or rejected
      addressLookup: true,    // requires Google API key if true
      timezone: true,         // requires Google API key if true
      // map: "map-canvas",      // interactive map element id (or options object)
      // staticMap: true         // map image URL (boolean or options object)
    };
    navigator.geolocation.getCurrentPosition((pos) => {
      this.setState({
        showSpinner: false
      })
      window.location.replace(`/results?lat=${pos.coords.latitude}&lng=${pos.coords.longitude}&radius=${default_radius}`)
    })
  }

  handleOnSuggestSelect(suggest) {
    window.location = `/results?lat=${suggest.location.lat}&lng=${suggest.location.lng}&radius=${default_radius}`
  }

  showSpinner() {
    return this.state.showSpinner
      ? <div id="spinner-wrap">
        <div className="spinner">
          <div className="dot1"></div>
          <div className="dot2"></div>
        </div>
      </div>
      : <div></div>
  }

  render() {
    return <div className="input-group">
      <span className="input-group-btn">
        <button className="btn btn-default" type="button" onClick={this.handleOnClick.bind(this)}>
          <i style={{
            fontSize: 18
          }} className="fa fa-map-marker" aria-hidden="true"></i>
        </button>
      </span>
      <Geosuggest
        style={{
          input: {
            height: 38
          }
        }}
        fixtures={fixtures}
        placeholder="Standort eingeben..."
        country="de"
        initialValue="Würzburg"
        onSuggestSelect={this.handleOnSuggestSelect}
      />
      {this.showSpinner()}
    </div>
  }
}

export default GeosuggestInput
