import React, { Component } from 'react'
import _ from 'lodash'

class Results extends Component {
  componentWillMount() {
    this.state = {
      filter: {
        radius: null,
        lat: null,
        lng: null
      }
    }
  }

  componentDidMount() {

  }

  renderResults() {
    let results = this.props.results.map((result, key) => {
      return <div className="card" key={key}>
        <div style={{
          position: 'absolute'
        }}>{key}</div>
        <div className="col-sm-2 col-md-2">
          <img className="card-img-top"
            src={result.image_url === "-"
              ? null
              : result.image_url}
            alt="Logo" />
        </div>
        <div className="col-sm-10 col-md-10">
          <div className="card-block">
            <h4 className="card-title col-sm-8">{result.name}</h4>
            <p className="card-text col-sm-4">Entfernung: {result.distance}m</p>
            <div className="col-sm-12">
              <span className="score">
                <span style={{
                  width: `${result.rating / 5 * 100}%`
                }}></span>
              </span>
            </div>
          </div>
        </div>
      </div>
    })
    return <div className="col-sm-7">
      <div className="" style={{ lineHeight: '40px'}}>
        Anzahl Restaurants: {this.props.results.length}
      </div>
      {results.length > 0
        ? results
        : <div>Wir haben leider keine Restaurants in deiner Nähe finden können 😭</div>
      }
    </div>
  }

  render() {
    return <div className="col-sm-12">
      <div className="col-sm-3 col-sm-offset-1">
        <div className="form-group">
          <div className="col-sm-5">
            <label
              style={{
                lineHeight: '34px'
              }}
              htmlFor="radius-select">Entfernung:</label>
          </div>
          <div className="col-sm-7">
            <select
              onChange={(e) => {
                this.props.distanceFilterChanged(e.target.value, this.props.user_location)
              }}
              className="form-control" id="radius-select">
              <option value="400">400m</option>
              <option value="600">600m</option>
              <option value="800">800m</option>
            </select>
          </div>
        </div>
      </div>
      {this.renderResults()}
    </div>
  }
}

export default Results
