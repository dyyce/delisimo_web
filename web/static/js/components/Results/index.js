import Results from './Results'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../../actions/results_actions'

export default connect(
  state => ({
    results: state.resultsReducer.results,
    user_location: state.resultsReducer.user_location
  }),
  dispatch => bindActionCreators(actions, dispatch)
)(Results)
