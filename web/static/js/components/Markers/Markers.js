import React, { Component } from 'react'
import ReactToolTip from 'react-tooltip'

const K_WIDTH = 20;
const K_HEIGHT = 20;

const styles = {
  markerBase: {
    position: 'absolute',
    width: K_WIDTH,
    height: K_HEIGHT,
    left: -K_WIDTH / 2,
    top: -K_HEIGHT / 2,
    borderRadius: K_HEIGHT,
    textAlign: 'center',
    color: '#3f51b5',
  },
  mainMarker: {
    backgroundColor: 'blue',
  },
  resultMarker: {
    backgroundColor: 'orange',
    zIndex: 1
  },
  resultMarkerHover: {
    backgroundColor: 'black',
    cursor: 'pointer',
    zIndex: 9999
  },
  innerIcon: {
    color: 'white',
    fontSize: 16,
    position: 'relative',
    top: -10
  }
}

export class ResultMarker extends Component {
  componentWillMount() {
    this.state = {
      hover: false
    }
  }

  render() {
    let rating =  this.props.item.rating / 5 * 100;
    let { item } = this.props
    let tooltip = `<div>
      <img src="${item.image_url}" alt="Logo" />
      <div class="tooltip-text">
        <div class="tooltip-name">${item.name}</div>
        <div class="tooltip-distance">${item.distance}m entfernt</div>
        
      </div>
    </div>`

    return <div
      onMouseEnter={() => this.setState({ hover: true})}
      onMouseLeave={() => this.setState({ hover: false})}
      data-tip={tooltip}
      style={Object.assign([], styles.markerBase, this.props.style, this.state.hover ? styles.resultMarkerHover : styles.resultMarker)}>
      <div style={styles.innerIcon}>{this.props.children}</div>
      <ReactToolTip
        class="result-marker" //check it @markers.css
        html={true}
        effect={"solid"}
        ></ReactToolTip>
    </div>
  }
}

export class MainMarker extends Component {
  render() {
    return <div style={Object.assign([], styles.mainMarker, styles.markerBase, this.props.style)}>
      <div style={styles.innerIcon}>{this.props.children}</div>
    </div>
  }
}
