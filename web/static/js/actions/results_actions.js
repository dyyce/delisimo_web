import * as types from './constants'
import socket from '../socket'

socket.connect()
let channel = socket.channel("page:results", {})
channel.join()
  .receive("ok", resp => { console.log("Joined successfully", resp) })
  .receive("error", resp => { console.log("Unable to join", resp) })

// payload : {results, user_location}
export const getResultsSuccess = (payload) => {
  return {
    type: types.GET_RESULTS_SUCCESS,
    payload
  }
}

export const sortByRating = (results) => {
  return {
    type: types.SORT_BY_RATING,
    results
  }
}

export const distanceFilterChanged = (radius, user_location) => {
  return dispatch => {
    dispatch(distanceFilterChangedRequest())
    channel.push("filter_changed", {radius, user_location})
  }
}

export const distanceFilterChangedRequest = () => {
  return {
    type: types.DISTANCE_FILTER_CHANGED_REQUEST,
  }
}

export const distanceFilterChangedSuccess = (results, user_location) => {
  return {
    type: types.DISTANCE_FILTER_CHANGED_SUCCESS,
    results,
    user_location
  }
}

export const distanceFilterChangedFailure = () => {
  return {
    type: types.DISTANCE_FILTER_CHANGED_FAILURE
  }
}
