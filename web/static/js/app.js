import "phoenix_html"
import socket from "./socket"
import React from "react"
import ReactDOM from "react-dom"
import { Provider } from 'react-redux'
import { createStore, compose, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import createLogger from 'redux-logger'

import reducers from './reducers'
import actions from './actions'
import {setupRealtime} from './realtime/events'

import Results from './components/Results'
import GeosuggestInput from './components/GeosuggestInput/'
import ResultsBar from './components/ResultsBar'

const loggerMiddleware = createLogger({
  level: 'info',
  collapsed: true,
});

const enhancer = compose(
  applyMiddleware(loggerMiddleware, thunk)
);

let store = createStore(reducers, enhancer)
setupRealtime(store, actions)

let resultsBar = document.getElementById('results-bar')
if(resultsBar) ReactDOM.render(
  <Provider store={store}>
    <ResultsBar/>
  </Provider>, resultsBar
)

let results = document.getElementById('results')
if(results) ReactDOM.render(
  <Provider store={store}>
    <Results/>
  </Provider>, results
)

let geosuggest = document.getElementById("geosuggest-input")
if(geosuggest) ReactDOM.render(
  <GeosuggestInput
  />, geosuggest
)
