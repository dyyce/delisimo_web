import _ from 'lodash'
import * as types from '../actions/constants'

const INIT_STATE = {
  results: [],
  user_location: {},
  orderType: 'desc'
}

const resultsReducer = (state = INIT_STATE, action) => {
  switch (action.type) {
    case types.SORT_BY_RATING:
      return Object.assign({}, state, {
        results: _.orderBy(state.results, 'rating', 'desc')
      })
    case types.DISTANCE_FILTER_CHANGED_SUCCESS:
      return Object.assign({}, state, {
        results: action.results,
        user_location: action.user_location
      })
    case types.GET_RESULTS_REQUEST:
      return Object.assign({}, state, {
        results: action.payload.results,
        user_location: action.payload.user_location
      })
    case types.GET_RESULTS_SUCCESS:
      return Object.assign({}, state, {
        results: action.payload.results,
        user_location: action.payload.user_location
      })
    case types.GET_RESULTS_FAILURE:
      return Object.assign({}, state, {
        error: action.error
      })
    default:
      return state
  }
}

export default resultsReducer
