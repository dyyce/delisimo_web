import { combineReducers } from 'redux'
import resultsReducer from './results_reducer'

const reducers = combineReducers({
  resultsReducer
})

export default reducers
