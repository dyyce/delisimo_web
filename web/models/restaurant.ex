defmodule Delisimo.Restaurant do
  use Delisimo.Web, :model
  import Ecto.Query

  @derive {Poison.Encoder, except: [:__meta__]}

  schema "restaurants" do
    field :name, :string
    field :yelp_website, :string
    field :yelp_id, :string
    field :city, :string
    field :street, :string
    field :zip, :integer
    field :country, :string
    field :price, :string
    field :rating, :float
    field :review_count, :integer
    field :image_url, :string
    field :phone, :string
    field :hours, :string
    field :coords, Geo.Point
    field :distance, :float, virtual: true

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :yelp_website, :yelp_id, :city, :street, :zip, :country, :price, :rating, :review_count, :image_url, :phone, :hours, :coords])
    # |> validate_required([:name, :yelp_id, :city, :country, :coords, :image_url, :hours])
  end

  def within(query, point, radius_in_m) do
    {lng, lat} = point.coordinates
    from(restaurant in query, where: fragment("ST_DWithin(?::geography, ST_SetSRID(ST_MakePoint(?, ?), ?), ?)", restaurant.coords, ^lng, ^lat, ^point.srid, ^radius_in_m))
  end

  def order_by_nearest(query, point) do
    {lng, lat} = point.coordinates
    from(restaurant in query, order_by: fragment("? <-> ST_SetSRID(ST_MakePoint(?,?), ?)", restaurant.coords, ^lng, ^lat, ^point.srid))
  end

  def select_with_distance(query, point) do
    {lng, lat} = point.coordinates
    from(restaurant in query,
         select: %{restaurant | distance: fragment("ST_Distance_Sphere(?, ST_SetSRID(ST_MakePoint(?,?), ?))", restaurant.coords, ^lng, ^lat, ^point.srid)})
  end
end
