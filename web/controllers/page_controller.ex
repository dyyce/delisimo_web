defmodule Delisimo.PageController do
  use Delisimo.Web, :controller

  alias Delisimo.Restaurant

  def index(conn, _params) do
    render conn, "index.html"
  end

  def results(conn, %{"lat" => lat, "lng" => lng, "radius" => radius}) do

    conn
    |> render "results.html"
  end
end
