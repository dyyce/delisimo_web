defmodule Delisimo.ResultsChannel do
  use Phoenix.Channel
  alias Delisimo.Restaurant
  alias Delisimo.Repo

  def join("page:results", _message, socket) do
    {:ok, socket}
  end

  def join("page:" <> _private_room_id, _params, _socket) do
    {:error, %{reason: "unauthorized"}}
  end

  def handle_in("req_results", %{"lat" => lat, "lng" => lng, "radius" => radius}, socket) do
    user_coords = %Geo.Point{coordinates: {String.to_float(lat), String.to_float(lng)}, srid: 4326}
    results = results_request %{radius: radius}, user_coords

    push socket, "res_results", %{results: results, user_location: %{lat: lat, lng: lng, radius: radius}}
    {:noreply, socket}
  end

  def handle_in("filter_changed", payload, socket) do
    # IO.puts "***"
    # IO.inspect payload
    # IO.puts "***"
    %{"radius" => radius, "user_location" => user_location} = payload
    user_coords = %Geo.Point{coordinates: {
      String.to_float(Map.get(user_location, "lat")),
      String.to_float(Map.get(user_location, "lng"))},
      srid: 4326}

    results = results_request %{radius: radius}, user_coords

    push socket, "filter_changed", %{user_location: user_location, results: results}
    {:noreply, socket}
  end

  def handle_out("req_results", payload, socket) do
    push socket, "res_results", payload
    {:noreply, socket}
  end

  defp results_request(filter, location) do
    results = Restaurant.within(Restaurant, location, String.to_integer filter.radius) |> Restaurant.order_by_nearest(location) |> Restaurant.select_with_distance(location) |> Repo.all

    Enum.map(results, fn(x) ->
      # IO.inspect Map.take(x, [:distance])
      {lat, lng} = Map.get(x, :coords) |> Map.get(:coordinates)
      %{
        "lat" => lat,
        "lng" => lng,
        "name" => Map.get(x, :name),
        "image_url" => Map.get(x, :image_url),
        "rating" => Map.get(x, :rating),
        "distance" => Float.to_string(Map.get(x, :distance), decimals: 0)
      }
    end)
  end

end
