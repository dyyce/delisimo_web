# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :delisimo,
  ecto_repos: [Delisimo.Repo],
  extensions: [{Geo.PostGIS.Extension, library: Geo}]

# Configures the endpoint
config :delisimo, Delisimo.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "E+P2yt9aJRz6HK/P4vlCNXTO+YV40JpoPofVuF4X+MKfh93xX7GJmN7i5AffkxgL",
  render_errors: [view: Delisimo.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Delisimo.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
