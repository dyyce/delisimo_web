use Mix.Config

# In this file, we keep production configuration that
# you likely want to automate and keep it away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or you later on).
config :delisimo, Delisimo.Endpoint,
  secret_key_base: "zenPdHNUD9b7qtRczuHRMC3vAS3+QzjbA4Y9lu6c04BQ7ZbBMifYXue1Jyt7zhq9"

# Configure your database
config :delisimo, Delisimo.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "delisimo_prod",
  password: "dsfdsf123",
  database: "delisimo_prod",
  pool_size: 20,
  extensions: [{Geo.PostGIS.Extension, library: Geo}]
