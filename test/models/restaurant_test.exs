defmodule Delisimo.RestaurantTest do
  use Delisimo.ModelCase

  alias Delisimo.Restaurant

  @valid_attrs %{city: "some content", country: "some content", hours: "some content", name: "some content", phone: "some content", street: "some content", yelp_id: "some content", zip: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Restaurant.changeset(%Restaurant{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Restaurant.changeset(%Restaurant{}, @invalid_attrs)
    refute changeset.valid?
  end
end
