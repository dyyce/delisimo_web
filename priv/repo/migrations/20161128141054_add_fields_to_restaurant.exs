defmodule Delisimo.Repo.Migrations.AddFieldsToRestaurant do
  use Ecto.Migration

  def change do
    alter table(:restaurants) do
      add :yelp_website, :string
      add :price, :string
      add :rating, :float
      add :review_count, :integer
      add :image_url, :string
    end
  end
end
