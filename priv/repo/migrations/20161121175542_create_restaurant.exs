defmodule Delisimo.Repo.Migrations.CreateRestaurant do
  use Ecto.Migration

  def change do
    create table(:restaurants) do
      add :name, :string
      add :yelp_id, :string
      add :city, :string
      add :street, :string
      add :zip, :integer
      add :country, :string
      add :phone, :string
      add :hours, :string
      add :coords, :geometry

      timestamps()
    end

  end
end
