# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Delisimo.Repo.insert!(%Delisimo.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Delisimo.Restaurant
alias Delisimo.Repo

defmodule Delisimo.Seeds do

  def store_it(row) do
    # IO.inspect row
    row = fix_coords(row)
    IO.inspect row
    row = fix_zip(row)
    IO.inspect row

    changeset = Restaurant.changeset(%Restaurant{}, row)
    IO.puts "+++ schreibe " <> row.name
    Repo.insert!(changeset)
    IO.puts "+++ OK: " <> row.name
  end

  def fix_coords(%{coords: coords} = row) do
    # IO.inspect coords
    cond do

      String.length(coords) <= 2 ->
        Map.update!(row,:coords,fn _ -> nil end)


      true ->
        coords_as_float_array = String.split(coords, ", ")
        |> Enum.map(fn(x) ->
          String.to_float(x)
        end)
        coords_as_point = %Geo.Point{coordinates: {List.first(coords_as_float_array), List.last(coords_as_float_array)}, srid: 4326}

        # IO.inspect coords_as_point
        Map.update!(row,:coords,fn _ -> coords_as_point end)

    end
  end

  def fix_zip(%{zip: zip} = row) do
    # IO.inspect zip
    cond do
      String.strip(zip) == "" -> Map.update!(row, :zip, fn _ -> nil end)
      true -> Map.update!(row,:zip, fn _ -> String.to_integer(zip) end)
    end
  end
end

# Würzburg
# File.stream!("/Users/fizzle/Desktop/dev/projects/elixir/delisimo/priv/repo/restaurants_wuerzburg.tsv")
File.stream!("./priv/repo/restaurants_wuerzburg.tsv")
|> Stream.drop(1)
|> CSV.decode(separator: ?\t, headers: [:yelp_id, :yelp_website, :type, :name, :city, :street, :zip, :country, :price, :rating, :review_count, :image_url, :coords, :phone, :hours])
|> Enum.each(&Delisimo.Seeds.store_it/1)

# Aschaffenburg
# File.stream!("/Users/fizzle/Desktop/dev/projects/elixir/delisimo/priv/repo/restaurants_aschaffenburg.tsv")
File.stream!("./priv/repo/restaurants_aschaffenburg.tsv")
|> Stream.drop(1)
|> CSV.decode(separator: ?\t, headers: [:yelp_id, :yelp_website, :type, :name, :city, :street, :zip, :country, :price, :rating, :review_count, :image_url, :coords, :phone, :hours])
|> Enum.each(&Delisimo.Seeds.store_it/1)
